function Hamburger(sizeBurger, stuffingBurger) {
    try {
        if (sizeBurger.size) {
            this.size = sizeBurger
        } else {
            throw new HamburgerException("Это не размер");
        }
        if (stuffingBurger.stuffing) {
            this.stuffing = stuffingBurger
        } else {
            throw new HamburgerException("Это не топпинг");
        }
    } catch (e) {
        console.log(e.message)
    }
    this.topping = []
}
Hamburger.prototype.addTopping = function (toppingBurger) {
    try {
        if ( toppingBurger.topping ) {
            let checkTopping = this.topping.some(( el ) => {
            return el === toppingBurger
            })
            if (!checkTopping) {
                this.topping.push(toppingBurger)
            } else {
                console.log('Такой топпинг уже есть в вашем бургере');
            }
        } else {
            throw new HamburgerException ("Вы добавляете не топпинг");
        }
    } catch (e) {
        console.log(e.message);
    }
}
Hamburger.prototype.removeTopping = function (toppingBurger) {
    try {
        if ( toppingBurger.topping ) {
            this.topping.forEach((toppingInBurger) => {
            if (toppingInBurger === toppingBurger) {
                const idx = this.topping.indexOf(toppingBurger)
                    this.topping.splice(idx)
                } else {
                console.log('у вас нет такого топпинга');
            }
            })
        } else {
            throw new HamburgerException ("Это не топпинг");
        }
    } catch (e) {
        console.log(e.message);
    }

}
Hamburger.prototype.getToppings = function () {
    console.log(this.topping);
}
Hamburger.prototype.getSize = function () {
    console.log(`You ordered ${this.size.size} burger`)
}
Hamburger.prototype.getStuffing = function () {
    console.log(`Stuffing your burger is ${this.stuffing.stuffing}`)
}
Hamburger.prototype.calculatePrice = function () {
    let currentPrice
    try {
        if ( this.size ) {
            if ( this.stuffing ) {
                currentPrice = this.size.price
                    + this.stuffing.price
                if ( this.topping.length ) {
                    this.topping.forEach((el) => {
                        currentPrice += el.price
                    })
                } else {
                    throw new HamburgerException("Для подсчета цены не хватает топпинга")
                }
                console.log(`Price your's burger - ${currentPrice}`)
            } else {
                throw new HamburgerException("Для подсчета цены не хватает начинки")
            }
        } else {
            throw new HamburgerException("Для подсчета цены не хватает бургера")
        }
    } catch (e) {
        console.log(e.message);
    }
    return currentPrice
}
Hamburger.prototype.calculateCalories = function () {
    let currentCalories
    try {
        if ( this.size ) {
            if ( this.stuffing ) {
                currentCalories = this.size.calories
                                    + this.stuffing.calories
                if ( this.topping.length ) {
                    this.topping.forEach((el) => {
                        currentCalories += el.calories
                    })
                } else {
                        throw new HamburgerException("Для подсчета цены не хватает топпинга")
                }
                    console.log(`Calories in your's burger - ${currentCalories}`)
            } else {
                throw new HamburgerException("Для подсчета цены не хватает начинки")
            }
        } else {
            throw new HamburgerException("Для подсчета цены не хватает бургера")
        }
    } catch (e) {
        console.log(e.message);
    }
    return currentCalories
}
function HamburgerException (message) {
   this.message = message;
}

Hamburger.SIZE_SMALL = { size : 'small', price : 50, calories : 20 };
Hamburger.SIZE_LARGE = { size : 'large', price : 100, calories : 40 };
Hamburger.STUFFING_CHEESE = { stuffing : 'cheese', price : 10, calories : 20 };
Hamburger.STUFFING_SALAD = {  stuffing : 'salad', price : 20, calories : 5 };
Hamburger.STUFFING_POTATO = { stuffing : 'potato', price : 15, calories : 10 };
Hamburger.TOPPING_MAYO = { topping : 'mayonnaise', price : 20, calories : 5 };
Hamburger.TOPPING_SPICE = { topping : 'spice', price : 15, calories : 0 };

let burger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_POTATO)

burger.addTopping(Hamburger.TOPPING_SPICE)
// burger.addTopping(Hamburger.TOPPING_MAYO)
burger.addTopping(Hamburger.TOPPING_MAYO)
// burger.removeTopping(Hamburger.TOPPING_SPICE)
// burger.removeTopping(Hamburger.TOPPING_MAYO)
// burger.getToppings()
// burger.getSize()
// burger.getStuffing()
burger.calculatePrice(burger)
burger.calculateCalories()

console.log(burger);